compass-normalize-plugin (7.0.1-3) unstable; urgency=medium

  * add patch 1001 to modernize to align with normalize.css v8.0.1

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 06 Mar 2021 17:14:45 +0100

compass-normalize-plugin (7.0.1-2) unstable; urgency=medium

  * use debhelper compatibility level 13 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.1
  * copyright info: update coverage
  * update source helper script copyright-check
  * stop depend on ruby ruby-sass
  * provide virtual package sass-stylesheets-normalize

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 02 Mar 2021 13:41:46 +0100

compass-normalize-plugin (7.0.1-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Simplify rules.
    Stop build-depend on cdbs.
  * Stop build-depend on dh-buildinfo.
  * Declare compliance with Debian Policy 4.3.0.
  * Set Rules-Requires-Root: no.
  * Update Vcs-* URLs: Maintenance moved to Salsa.
  * Update copyright info:
    + Drop superfluous alternate Source URL.
    + Extend coverage of packaging.
  * Update watch file:
    + Simplify regular expressions.
    + Rewrite usage comment.
  * Tighten lintian overrides regarding License-Reference.
  * Unfuzz patches.
  * Include sass files below /usr/share/sass.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 Mar 2019 18:49:23 +0100

compass-normalize-plugin (7.0.0-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Modernize cdbs:
    + Drop upstream-tarball hints: Use gbp import-orig --uscan.
    + Do copyright-check in maintainer script (not during build).
      Stop build-depend on licensecheck.
  * Update copyright info:
    + Extend coverage for myself.
    + Use https protocol in file format URL.
  * Declare compliance with Debian Policy 4.1.0.
  * Advertise DEP3 format in patch headers.
  * Update package relations:
    + Relax to (build-)depend unversioned on ruby-sass: Needed version
      satisfied even in oldstable.
  * Relax cleanup of Ruby, to limit build-depencies sloppily needed
    outside a build chroot.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 23 Aug 2017 18:28:28 +0200

compass-normalize-plugin (6.0.0-2) unstable; urgency=medium

  * Fix watch file.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 23 Jan 2017 16:01:35 +0100

compass-normalize-plugin (6.0.0-1) unstable; urgency=medium

  [ upstream ]
  * Switch to JohnAlbin implementation.

  [ Jonas Smedegaard ]
  * Fix timestamp in patch 2001 header.
  * Update watch file:
    + Switch to JohnAlbin implementation.
    + Bump to version 4.
    + Add usage comment.
    + Use github pattern from documentation.
  * Update git-buildpage config:
    + Avoid git- prefix.
    + Filter any .git* file.
  * Update copyright info:
    + Reflect switch to JohnAlbin implementation (Expat license).
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage of Debian packaging.
  * Update homepage and rewrite short and long descriptions to reflect
    switch to JohnAlbin implementation.
  * Modernize Vcs-* fields:
    + Use https protocol.
    + Use git subdir.
    + Add .git suffix for Vcs-Git URL.
  * Declare compliance with Debian Policy 3.9.8.
  * Rewrite patch 2001 for JohnAlbin implementation.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Add NEWS file about switch to JohnAlbin implementation.
  * Update package relations:
    + Relax dependency on ruby-sass.
    + Stop fallback-depend on ruby-compass.
    + Build-depend on licensecheck (not devscripts).
  * Install fork-versions as examples.
  * Add patch 2002 to avoid use of git in gemspec.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 22 Jan 2017 18:30:05 +0100

compass-normalize-plugin (1.5~20140508-1) unstable; urgency=low

  * Initial release.
    Closes: bug#766507.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Oct 2014 18:50:18 +0200
